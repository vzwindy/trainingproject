﻿using ApplicationCore.Common;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IAsyncRepository<T> where T : class
    {
        Task<T> GetByIdAsync(int id);
        Task<T> GetFirstAsync(RepoSpec<T> spec, bool InactiveIncluded = false);
        Task<T> GetFirstAsync(Expression<Func<T, bool>> criteria, bool InactiveIncluded = false);
        Task<T> AddAsync(T entity);
        Task<T> UpdateAsync(T entity);
        Task DeleteAsync(T entity, bool isForced = false);
    }
}
