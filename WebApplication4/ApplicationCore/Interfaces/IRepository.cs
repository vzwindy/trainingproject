﻿using ApplicationCore.Common;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace ApplicationCore.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T GetFirst(RepoSpec<T> spec, bool InactiveIncluded = false);
        IQueryable<T> GetAll(bool InactiveIncluded = false, int pageNumber = 0, int pageSize = 0);
        IQueryable<T> Get(RepoSpec<T> spec, bool InactiveIncluded = false);
        IQueryable<T> Get(Expression<Func<T, bool>> criteria, bool InactiveIncluded = false, int pageNumber = 0, int pageSize = 0);
        T GetFirst(Expression<Func<T, bool>> criteria, bool InactiveIncluded = false);
        T Add(T entity);
        T Update(T entity);
        void Delete(T entity, bool isForced = false);
    }
}
