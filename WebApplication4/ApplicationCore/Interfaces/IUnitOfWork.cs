﻿
namespace ApplicationCore.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<T> Repo<T>() where T : class;
        IAsyncRepository<T> AsyncRepo<T>() where T : class;
        void Commit();
        void Rollback();
    }
}
