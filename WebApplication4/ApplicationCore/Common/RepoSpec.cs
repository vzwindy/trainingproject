﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ApplicationCore.Common
{
    public class RepoSpec<T>
    {
        public RepoSpec(Expression<Func<T, bool>> criteria, bool isQueryType = false, int pageNumber = 0, int pageSize = 0)
        {
            Criteria = criteria;
            IsQueryType = isQueryType;
            PageNumber = pageNumber;
            PageSize = pageSize;
        }
        public RepoSpec(bool isQueryType = false, int pageNumber = 0, int pageSize = 0)
        {
            IsQueryType = isQueryType;
            PageNumber = pageNumber;
            PageSize = pageSize;
        }
        public bool IsQueryType { get; }
        public Expression<Func<T, bool>> Criteria { get; }
        public string RawSql { get; set; }
        public object[] SqlParameters { get; set; }
        public List<Expression<Func<T, object>>> Orders { get; } = new List<Expression<Func<T, object>>>();
        public List<Expression<Func<T, object>>> OrderDescs { get; } = new List<Expression<Func<T, object>>>();
        public List<Expression<Func<T, object>>> Includes { get; } = new List<Expression<Func<T, object>>>();
        public List<string> IncludeStrings { get; } = new List<string>();
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
