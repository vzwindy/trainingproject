﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Province
    {
        [Key]
        public int ProvinceId { get; set; }
        [Required]
        public string ProvinceName { get; set; }

        [ForeignKey("ProvinceFK")]
        public ICollection<Student> Students { get; set; }
    }
}
