﻿using ApplicationCore.Common;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class StudentService : IStudentService
    {
        private readonly IUnitOfWork _unitOfWork;

        public StudentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task CreateStudent(Student student)
        {
            await _unitOfWork.AsyncRepo<Student>().AddAsync(student);
        }

        public async Task DeleteStudent(Student student)
        {
            await _unitOfWork.AsyncRepo<Student>().DeleteAsync(student);
        }

        public List<Student> GetAll()
        {
            return _unitOfWork.Repo<Student>().GetAll().ToList();
        }

        public async Task<Student> GetStudentById(int id)
        {
            return await _unitOfWork.AsyncRepo<Student>().GetByIdAsync(id);
        }

        public async Task UpdateStudent(Student student)
        {
            await _unitOfWork.AsyncRepo<Student>().UpdateAsync(student);
        }
    }
}
