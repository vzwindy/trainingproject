﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication4.Controllers
{   
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IStudentService _iStudentService;
        private readonly IProvinceService _iProvinceService;

        public StudentController(IStudentService iStudentService,IProvinceService provinceService)
        {
            _iStudentService = iStudentService;
            _iProvinceService = provinceService;
        }

        [HttpGet("{id}")]
        public async Task<string[]> GetInfoStudent(int id)
        {
            var student_got =  await _iStudentService.GetStudentById(id);
            var province_name = await _iProvinceService.GetProvinceNameById(student_got.Id);
            return new string[]
            {
                student_got.Id.ToString(),
                student_got.Name,
                student_got.PhoneNumber.ToString(),
                student_got.Email,
                student_got.Address,
                province_name.ProvinceName
            };
        }

        [HttpPost]
        public async Task AddStudent(Student student)
        {
            await _iStudentService.CreateStudent(student);
        }
        [HttpGet]
        public List<Student> GetAll()
        {
            return _iStudentService.GetAll();
        }
        [HttpPut]
        public async Task UpdateStudent(Student student)
        {
            await _iStudentService.UpdateStudent(student);
        }
        [HttpDelete]
        public async Task DeleteStudent(Student student)
        {
            await _iStudentService.DeleteStudent(student);
        }
    }
}