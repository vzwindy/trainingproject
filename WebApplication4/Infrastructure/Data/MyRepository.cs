﻿using ApplicationCore.Common;
using ApplicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class MyRepository<T> : IRepository<T>, IAsyncRepository<T> where T : class
    {
        protected readonly MyContext _dbContext;

        public MyRepository(MyContext dbContext)
        {
            _dbContext = dbContext;
        }

        public T GetFirst(RepoSpec<T> spec, bool InactiveIncluded = false)
        {
            return Get(spec, InactiveIncluded).FirstOrDefault();
        }

        public T GetFirst(Expression<Func<T, bool>> criteria, bool InactiveIncluded = false)
        {
            return Get(criteria, InactiveIncluded).FirstOrDefault();
        }

        public async Task<T> GetFirstAsync(RepoSpec<T> spec, bool InactiveIncluded = false)
        {
            return await Get(spec, InactiveIncluded).FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstAsync(Expression<Func<T, bool>> criteria, bool InactiveIncluded = false)
        {
            return await Get(criteria, InactiveIncluded).FirstOrDefaultAsync();
        }

        public IQueryable<T> GetAll(bool InactiveIncluded = false, int pageNumber = 0, int pageSize = 0)
        {
            var maxPageSize = 100;
            if (pageSize > maxPageSize) throw new Exception("Your page size number is invalid");

            if (InactiveIncluded)
            {
                return _dbContext.Set<T>().IgnoreQueryFilters();
            }
            if (pageNumber == 0 && pageSize == 0) return _dbContext.Set<T>();
            return _dbContext.Set<T>().Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> criteria, bool InactiveIncluded = false, int pageNumber = 0, int pageSize = 0)
        {
            var maxPageSize = 100;
            if (pageSize > maxPageSize) throw new Exception("Your page size number is invalid");

            IQueryable<T> result;
            if (InactiveIncluded)
            {
                result = _dbContext.Set<T>().IgnoreQueryFilters().Where(criteria);
            }
            else
            {
                result = _dbContext.Set<T>().Where(criteria);
            }
            if (pageNumber == 0 && pageSize == 0) return result;
            return result.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public IQueryable<T> Get(RepoSpec<T> spec, bool InactiveIncluded = false)
        {
            var maxPageSize = 100;
            if (spec.PageSize > maxPageSize) throw new Exception("Your page size number is invalid");

            // fetch a Queryable that includes all expression-based includes
            IQueryable<T> queryableResult;
            if (spec.IsQueryType)
                queryableResult = _dbContext.Query<T>().AsQueryable();
            else
                queryableResult = _dbContext.Set<T>().AsQueryable();

            var queryableResultWithIncludes = spec.Includes
                .Aggregate(queryableResult,
                    (current, include) => current.Include(include));
            // modify the IQueryable to include any string-based include statements
            var result2 = spec.IncludeStrings
                .Aggregate(queryableResultWithIncludes,
                    (current, include) => current.Include(include));

            // modify the IQueryable to include any expression-based orders
            var result3 = spec.Orders
                .Aggregate(result2,
                    (current, order) => current.OrderBy(order));

            var result4 = spec.OrderDescs
                .Aggregate(result3,
                    (current, order) => current.OrderByDescending(order));
            IQueryable<T> result5, result6;
            // return the result of the query using the specification's criteria expression
            if (InactiveIncluded) result5 = result4.IgnoreQueryFilters();
            else result5 = result4;

            if (spec.Criteria != null) result6 = result5.Where(spec.Criteria);
            else if (!string.IsNullOrEmpty(spec.RawSql)) result6 = result5.FromSql(spec.RawSql, spec.SqlParameters);
            else result6 = result5;

            if (spec.PageNumber == 0 && spec.PageSize == 0) return result6;
            return result6.Skip((spec.PageNumber - 1) * spec.PageSize).Take(spec.PageSize);
        }

        public T Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            _dbContext.SaveChanges();

            return entity;
        }
        public async Task<T> AddAsync(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public T Update(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
            return entity;
        }
        public async Task<T> UpdateAsync(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        public void Delete(T entity, bool isForced = false)
        {
            _dbContext.Set<T>().Remove(entity);
            _dbContext.SaveChanges();
        }
        public async Task DeleteAsync(T entity, bool isForced = false)
        {
            _dbContext.Set<T>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }
    }
}
