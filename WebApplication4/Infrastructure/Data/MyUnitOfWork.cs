﻿using ApplicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class MyUnitOfWork : IUnitOfWork
    {
        private readonly MyContext _context;

        private readonly Hashtable _hashRepository;

        private readonly Hashtable _hashAsyncRepository;

        public MyUnitOfWork(MyContext context)
        {
            _context = context;
            _hashRepository = new Hashtable();
            _hashAsyncRepository = new Hashtable();
        }

        public IRepository<T> Repo<T>() where T : class
        {
            var key = typeof(T).Name;
            if (!_hashRepository.Contains(key))
            {
                var repositoryType = typeof(MyRepository<>);
                var repository = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), _context);
                _hashRepository[key] = repository;
            }

            return (IRepository<T>)_hashRepository[key];
        }

        private void DiscardChanges()
        {
            foreach (var entry in _context.ChangeTracker.Entries().Where(e => e.State != EntityState.Unchanged).ToList())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    // If the EntityState is the Deleted, reload the date from the database.   
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }

        public IAsyncRepository<T> AsyncRepo<T>() where T : class
        {
            var key = typeof(T).Name;
            if (!_hashAsyncRepository.Contains(key))
            {
                var repositoryType = typeof(MyRepository<>);
                var repository = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), _context);
                _hashAsyncRepository[key] = repository;
            }

            return (IAsyncRepository<T>)_hashAsyncRepository[key];
        }

        public void Commit()
        {
            _context.Database.CurrentTransaction.Commit();
        }

        public void Rollback()
        {
            _context.Database.CurrentTransaction.Rollback();
            DiscardChanges();
        }
    }
}