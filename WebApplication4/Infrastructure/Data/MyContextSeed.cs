﻿using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data
{
    public static class MyContextSeed
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Province>().HasData(
                new Province
                {
                    ProvinceId=1,
                    ProvinceName="Demo"
                },
                new Province
                {
                    ProvinceId=2,
                    ProvinceName="Demo Test"
                }
                );
        }
    }
}
